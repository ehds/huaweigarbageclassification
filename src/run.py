import argparse
import os
from moxing.framework import file

parser = argparse.ArgumentParser(description='HW GARBAGE CALSSFICATION')
parser.add_argument('--mode','-m',default='train')

parser.add_argument('--local_data_root',default='/cache',
                    help='a directory used for transfer data between local path and OBS path')
# params for train
parser.add_argument('--data_url',default='',help='the training data path')
parser.add_argument('--restore_model_path',default='',help='a history model you have trained, you can load it and continue trainging')
parser.add_argument('--train_url',default='',help='the path to save training outputs')
parser.add_argument('--keep_weights_file_num',default=20,type=int,
                    help='the max num of weights files keeps, if set -1, means infinity')

parser.add_argument('--num_classes',default=0,type=int,help='the num of classes which your task should classify')
parser.add_argument('--input_size', default=224, type=int,help='the input image size of the model')
parser.add_argument('--batch_size', default=16, type=int,help='batch size')
parser.add_argument('--learning_rate', default=1e-4, type=float,help='learning rate')
parser.add_argument('--max_epochs', default=5, type=int,help='max epochs')

# params for save model
parser.add_argument('--deploy_script_path',default='',
                           help='a path which contain config.json and customize_service.py, '
                           'if it is set, these two scripts will be copied to {train_url}/model directory')
parser.add_argument('--freeze_weights_file_path',default='',
                            help='if it is set, the specified h5 weights file will be converted as a pb model, '
                           'only valid when {mode}=save_pb')

# params for evaluation
parser.add_argument('--eval_weights_path',default='',help='weights file path need to be evaluate')
parser.add_argument('--eval_pb_path', default='',help='a directory which contain pb file needed to be evaluate')
parser.add_argument('--test_data_url', default='',help='the test data path on obs')

parser.add_argument('--data_local',default='',help='the train data path on local')
parser.add_argument('--train_local', default='',help='the training output results on local')
parser.add_argument('--test_data_local',default='',help='the test data path on local')
parser.add_argument('--tmp',default='', help='a temporary path on local')
parser.add_argument('--init_method',default='', help='nothing')

# 离线resnet模型
parser.add_argument('--resnet_model_local',default='',help='pretrained resnet_model local path')
parser.add_argument('--resnet_model_url',default='',help='pretrained resnet_model remote path')

args = parser.parse_args()

def check_args(FLAGS):
    if FLAGS.mode not in ['train', 'save_pb', 'eval']:
        raise Exception('FLAGS.mode error, should be train, save_pb or eval')
    if FLAGS.num_classes == 0:
        raise Exception('FLAGS.num_classes error, '
                        'should be a positive number associated with your classification task')

    if FLAGS.mode == 'train':
        if FLAGS.data_url == '':
            raise Exception('you must specify FLAGS.data_url')
        if not file.exists(FLAGS.data_url):
            raise Exception('FLAGS.data_url: %s is not exist' % FLAGS.data_url)
        if FLAGS.restore_model_path != '' and (not file.exists(FLAGS.restore_model_path)):
            raise Exception('FLAGS.restore_model_path: %s is not exist' % FLAGS.restore_model_path)
        if file.is_directory(FLAGS.restore_model_path):
            raise Exception('FLAGS.restore_model_path must be a file path, not a directory, %s' % FLAGS.restore_model_path)
        if FLAGS.train_url == '':
            raise Exception('you must specify FLAGS.train_url')
        elif not file.exists(FLAGS.train_url):
            file.make_dirs(FLAGS.train_url)
        if FLAGS.deploy_script_path != '' and (not file.exists(FLAGS.deploy_script_path)):
            raise Exception('FLAGS.deploy_script_path: %s is not exist' % FLAGS.deploy_script_path)
        if FLAGS.deploy_script_path != '' and file.exists(FLAGS.train_url + '/model'):
            raise Exception(FLAGS.train_url +
                            '/model is already exist, only one model directoty is allowed to exist')
        if FLAGS.test_data_url != '' and (not file.exists(FLAGS.test_data_url)):
            raise Exception('FLAGS.test_data_url: %s is not exist' % FLAGS.test_data_url)

    if FLAGS.mode == 'save_pb':
        if FLAGS.deploy_script_path == '' or FLAGS.freeze_weights_file_path == '':
            raise Exception('you must specify FLAGS.deploy_script_path '
                            'and FLAGS.freeze_weights_file_path when you want to save pb')
        if not file.exists(FLAGS.deploy_script_path):
            raise Exception('FLAGS.deploy_script_path: %s is not exist' % FLAGS.deploy_script_path)
        if not file.is_directory(FLAGS.deploy_script_path):
            raise Exception('FLAGS.deploy_script_path must be a directory, not a file path, %s' % FLAGS.deploy_script_path)
        if not file.exists(FLAGS.freeze_weights_file_path):
            raise Exception('FLAGS.freeze_weights_file_path: %s is not exist' % FLAGS.freeze_weights_file_path)
        if file.is_directory(FLAGS.freeze_weights_file_path):
            raise Exception('FLAGS.freeze_weights_file_path must be a file path, not a directory, %s ' % FLAGS.freeze_weights_file_path)
        if file.exists(FLAGS.freeze_weights_file_path.rsplit('/', 1)[0] + '/model'):
            raise Exception('a model directory is already exist in ' + FLAGS.freeze_weights_file_path.rsplit('/', 1)[0]
                            + ', please rename or remove the model directory ')

    if FLAGS.mode == 'eval':
        if FLAGS.eval_weights_path == '' and FLAGS.eval_pb_path == '':
            raise Exception('you must specify FLAGS.eval_weights_path '
                            'or FLAGS.eval_pb_path when you want to evaluate a model')
        if FLAGS.eval_weights_path != '' and FLAGS.eval_pb_path != '':
            raise Exception('you must specify only one of FLAGS.eval_weights_path '
                            'and FLAGS.eval_pb_path when you want to evaluate a model')
        if FLAGS.eval_weights_path != '' and (not file.exists(FLAGS.eval_weights_path)):
            raise Exception('FLAGS.eval_weights_path: %s is not exist' % FLAGS.eval_weights_path)
        if FLAGS.eval_pb_path != '' and (not file.exists(FLAGS.eval_pb_path)):
            raise Exception('FLAGS.eval_pb_path: %s is not exist' % FLAGS.eval_pb_path)
        if not file.is_directory(FLAGS.eval_pb_path) or (not FLAGS.eval_pb_path.endswith('model')):
            raise Exception('FLAGS.eval_pb_path must be a directory named model '
                            'which contain saved_model.pb and variables, %s' % FLAGS.eval_pb_path)
        if FLAGS.test_data_url == '':
            raise Exception('you must specify FLAGS.test_data_url when you want to evaluate a model')
        if not file.exists(FLAGS.test_data_url):
            raise Exception('FLAGS.test_data_url: %s is not exist' % FLAGS.test_data_url)

def main(FLAGS=None):
    #check_args(FLAGS)

    #Create some local cache directories used for transfer data between local path and OBS path
    if not FLAGS.data_url.startswith('s3://'):
        FLAGS.data_local = FLAGS.data_url
    else:
        FLAGS.data_local = os.path.join(FLAGS.local_data_root, 'train_data/')
        if not os.path.exists(FLAGS.data_local):
            file.copy_parallel(FLAGS.data_url, FLAGS.data_local)
        else:
            print('FLAGS.data_local: %s is already exist, skip copy' % FLAGS.data_local)
        
    if not FLAGS.train_url.startswith('s3://'):
        FLAGS.train_local = FLAGS.train_url
    else:
        FLAGS.train_local = os.path.join(FLAGS.local_data_root, 'model_snapshots/')
        print("train url not startswith s3://")
        if not os.path.exists(FLAGS.train_local):
            os.mkdir(FLAGS.train_local)
    
    if not FLAGS.test_data_url.startswith('s3://'):
        FLAGS.test_data_local = FLAGS.test_data_url
    else:
        FLAGS.test_data_local = os.path.join(FLAGS.local_data_root, 'test_data/')
        if not os.path.exists(FLAGS.test_data_local):
            file.copy_parallel(FLAGS.test_data_url, FLAGS.test_data_local)
        else:
            print('FLAGS.test_data_local: %s is already exist, skip copy' % FLAGS.test_data_local)

    if not FLAGS.resnet_model_url.startswith('s3://'):
        FLAGS.resnet_model_local = FLAGS.resnet_model_url
    else:
        FLAGS.resnet_model_local = os.path.join(FLAGS.local_data_root, 'resnet_model/resnet101.pth')
        if not os.path.exists(FLAGS.resnet_model_local):
            file.copy_parallel(FLAGS.resnet_model_url, FLAGS.resnet_model_local)
        else:
            print('FLAGS.test_data_local: %s is already exist, skip copy' % FLAGS.resnet_model_local)

    # FLAGS.tmp = os.path.join(FLAGS.local_data_root, 'tmp/')
    # if not os.path.exists(FLAGS.tmp):
    #     os.mkdir(FLAGS.tmp)


    if FLAGS.mode == 'train':
        from train import train_model
        print("train...")
        print(args.train_local)
        train_model(FLAGS)
        
    # elif FLAGS.mode == 'save_pb':
    #     from save_model import load_weights_save_pb
    #     load_weights_save_pb(FLAGS)
    # elif FLAGS.mode == 'eval':
    #     from eval import eval_model
    #     eval_model(FLAGS)

if __name__ == "__main__":
    args = parser.parse_args()
    main(args)
