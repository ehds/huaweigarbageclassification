# -*- coding: utf-8 -*-
import torch
from torchvision import transforms
from glob import glob
from PIL import Image
import os
from glob import glob
import codecs
import numpy as np

class ImageDataSet( torch.utils.data.Dataset):
	def __init__(self, img_dir, dataset = '',transform=None):
		self.image_paths,self.labels = self.getImageInfo(img_dir)
		self.transform = transform

	def getImageInfo(self,img_dir):
		# get all the lable.txt
		label_files = glob(os.path.join(img_dir, '*.txt'))
		# random.shuffle(label_files)
		img_paths = []
		labels = []
		for index, file_path in enumerate(label_files):
			with codecs.open(file_path, 'r', 'utf-8') as f:
				line = f.readline()
			line_split = line.strip().split(', ')
			if len(line_split) != 2:
				print('%s contain error lable' % os.path.basename(file_path))
				continue
			img_name = line_split[0]
			label = int(line_split[1])
			img_paths.append(os.path.join(img_dir, img_name))
			labels.append(label)
		return img_paths,labels

	def __getitem__(self,index):
		img_path = self.image_paths[index]
		img = None
		with Image.open(img_path) as f:
			img = f.convert('RGB')
		if self.transform is not None:
			img = self.transform(img)

		return img,self.labels[index]

	def __len__(self):
		return len(self.labels)

def ImageDataLoader(train_data_dir,batch_size,num_classes,image_size):
    dataset = ImageDataSet(img_dir=train_data_dir,transform=transforms.Compose([
					        # transforms.RandomResizedCrop(image_size),
					        # transforms.RandomHorizontalFlip(),
					        transforms.CenterCrop(224),
							transforms.ToTensor(),
							transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                           ]))
    print(len(dataset))
    # 数据集加载器
    dataloader = torch.utils.data.DataLoader(dataset,batch_size=batch_size,
                                                shuffle=True, num_workers=4)
    
    return dataloader


if __name__ == "__main__":
	dataloader = ImageDataLoader('E:\\garbage_classify\\garbage_classify\\train_data',16,40,224)
	inputs, labels = next(iter(dataloader))
	print(inputs[0].shape)
	# cc = torch.randn(16,40)
	# _,preds = torch.max(cc, 1)
	# print(labels,preds)
	# print(labels == preds)