# -*- coding: utf-8 -*-
import os
import torch
from train import model_fn
from moxing.framework import file

def save_pt_model(FLAGS, model):
    if FLAGS.mode == 'train':
        pb_save_dir_local = FLAGS.train_local
        pb_save_dir_obs = FLAGS.train_url
    elif FLAGS.mode == 'save_pt':
        freeze_weights_file_dir = FLAGS.freeze_weights_file_path.rsplit('/', 1)[0]
        if freeze_weights_file_dir.startswith('s3://'):
            pb_save_dir_local = '/cache/tmp'
            pb_save_dir_obs = freeze_weights_file_dir
        else:
            pb_save_dir_local = freeze_weights_file_dir
            pb_save_dir_obs = pb_save_dir_local

    model_name = 'resnet.pth'
    model_save_dir = os.path.join(pb_save_dir_local, 'model')
    if not os.path.exists(model_save_dir):
            os.mkdir(model_save_dir)

    torch.save(model, os.path.join(model_save_dir,model_name))
    print('save pb to local path success')

    if pb_save_dir_obs.startswith('s3://'):
        file.copy_parallel(os.path.join(pb_save_dir_local, 'model'),
                               os.path.join(pb_save_dir_obs, 'model'))
        print('copy pb to %s success' % pb_save_dir_obs)

    file.copy(os.path.join(FLAGS.deploy_script_path, 'config.json'),
                  os.path.join(pb_save_dir_obs, 'model/config.json'))
    file.copy(os.path.join(FLAGS.deploy_script_path, 'customize_service.py'),
                  os.path.join(pb_save_dir_obs, 'model/customize_service.py'))
    if file.exists(os.path.join(pb_save_dir_obs, 'model/config.json')) and \
            file.exists(os.path.join(pb_save_dir_obs, 'model/customize_service.py')):
        print('copy config.json and customize_service.py success')
    else:
        print('copy config.json and customize_service.py failed')