import ast
import numpy as np
from PIL import Image
import torch
from torchvision import transforms
from model_service.pytorch_model_service import PTServingBaseService
import urllib2
print "downloading with urllib2"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class garbage_classify_service(PTServingBaseService):
    def __init__(self, model_name, model_path):
        # these three parameters are no need to modify
        self.model_name = model_name
        # self.model_path = os.path.join(model_path,'resnet.pth')
        self.model_path = model_path
        self.input_size = 224  # the input image size of the model
        self.model = torch.load(self.model_path,map_location='cpu')

        self.label_id_name_dict = \
            {
                "0": "其他垃圾/一次性快餐盒",
                "1": "其他垃圾/污损塑料",
                "2": "其他垃圾/烟蒂",
                "3": "其他垃圾/牙签",
                "4": "其他垃圾/破碎花盆及碟碗",
                "5": "其他垃圾/竹筷",
                "6": "厨余垃圾/剩饭剩菜",
                "7": "厨余垃圾/大骨头",
                "8": "厨余垃圾/水果果皮",
                "9": "厨余垃圾/水果果肉",
                "10": "厨余垃圾/茶叶渣",
                "11": "厨余垃圾/菜叶菜根",
                "12": "厨余垃圾/蛋壳",
                "13": "厨余垃圾/鱼骨",
                "14": "可回收物/充电宝",
                "15": "可回收物/包",
                "16": "可回收物/化妆品瓶",
                "17": "可回收物/塑料玩具",
                "18": "可回收物/塑料碗盆",
                "19": "可回收物/塑料衣架",
                "20": "可回收物/快递纸袋",
                "21": "可回收物/插头电线",
                "22": "可回收物/旧衣服",
                "23": "可回收物/易拉罐",
                "24": "可回收物/枕头",
                "25": "可回收物/毛绒玩具",
                "26": "可回收物/洗发水瓶",
                "27": "可回收物/玻璃杯",
                "28": "可回收物/皮鞋",
                "29": "可回收物/砧板",
                "30": "可回收物/纸板箱",
                "31": "可回收物/调料瓶",
                "32": "可回收物/酒瓶",
                "33": "可回收物/金属食品罐",
                "34": "可回收物/锅",
                "35": "可回收物/食用油桶",
                "36": "可回收物/饮料瓶",
                "37": "有害垃圾/干电池",
                "38": "有害垃圾/软膏",
                "39": "有害垃圾/过期药物"
            }

    def preprocess_img(self, img):
        """
        image preprocessing
        you can add your special preprocess method here
        """
        # resize_scale = self.input_size / max(img.size[:2])
        # img = img.resize((int(img.size[0] * resize_scale), int(img.size[1] * resize_scale)))
        # img = img.convert('RGB')
        # img = np.array(img)
        # img = img[:, :, ::-1]
        # img = self.center_img(img, self.input_size)
        transform=transforms.Compose([
                            # transforms.RandomResizedCrop(224),
                            # transforms.RandomHorizontalFlip(),
                            transforms.CenterCrop(224),
                            transforms.ToTensor(),
                            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                           ])

        return transform(img)

    def _preprocess(self, data):
        preprocessed_data = {}
        for k, v in data.items():
            for file_name, file_content in v.items():
                img = Image.open(file_content)
                img = self.preprocess_img(img)
                preprocessed_data[k] = img
        return preprocessed_data

    def _inference(self, data):
        """
        model inference function
        Here are a inference example of resnet, if you use another model, please modify this function
        """
        img = data['input_img']

        url = 'http://baidu.com' 
		f = urllib2.urlopen(url) 
		data = f.read() 
		with open("/tmp/cache", "wb") as code:     
			code.write(data)
			print("write success")

        img = img.unsqueeze(0)
        outputs = self.model(img)
        _, preds = torch.max(outputs, 1)

        if preds is not None:
            result = {'result': self.label_id_name_dict[str(preds[0].item())]}
        else:
            result = {'result': 'predict score is None'}
        return result

    def _postprocess(self, data):
        return data
